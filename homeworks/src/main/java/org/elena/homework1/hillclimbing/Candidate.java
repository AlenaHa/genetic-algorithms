package org.elena.homework1.hillclimbing;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 10/19/16.
 *
 * This defines the model for a candidate.
 */
public class Candidate {

    /**
     * Genes keeps a list of genes for function
     */
    private List<Float> genes;

    /**
     * The public constructor initializes the list of genes.
     */
    public Candidate() {
        genes = new ArrayList<Float>();
    }

    /**
     * This method allows us to add new genes in the private list.
     *
     * @param gene Reprezents the gene to be added in the list.
     * @return Returns the added gene in list.
     */
    public boolean add(Float gene) {
        return genes.add(gene);
    }

    /**
     * This method helps to access the private genes list.
     * @return Returns the genes list.
     */
    public List<Float> getGenes() {
        return genes;
    }

    public void removeGene(int index) {
        this.genes.remove(index);
    }

    public void clearAll() {
        this.genes.clear();
    }

    /**
     * This method sets the list of genes with the one given as parameter.
     * @param genes To be set as genes.
     */
    public void setGenes(List<Float> genes) {
        this.genes = genes;
    }

    @Override
    public String toString() {
        String output = "";

        for (Float gene : genes) {
            output = output + "[" + gene + "] ";
        }
        return output;
    }
}
