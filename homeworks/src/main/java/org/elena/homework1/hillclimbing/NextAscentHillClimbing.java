package org.elena.homework1.hillclimbing;

import org.elena.homework0.hillclimbing.function.Function;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 10/19/16.
 *
 * Calculates the best candidate that gives the bestScore using Next Ascent HillClimbing method.
 */
public class NextAscentHillClimbing {
    /**
     * This function generates the best possible candidate that leads to the maximum fitness score in the
     * given number of iterations.
     *
     * @param function              Gives the function type on which we calculate the Hill Climbing method.
     * @param maxNumberOfIterations Gives the maximum number of iterations to be done.
     * @param numberOfGenes         Sets the number of genes to be random generated.
     * @return The best possible candidate that leads to the maximum fitness score.
     */
    public Candidate calculate(Function function, Integer maxNumberOfIterations, Integer numberOfGenes) {
        Float fitnessScore;
        List<Candidate> minHilltops = new ArrayList<Candidate>();

        for (int i = 0; i < maxNumberOfIterations; i++) {
            // First, the random genes must be generated.
            Candidate minHilltop = function.generateRandomCandidate(numberOfGenes);
            // The first fitness score is calculated using the random generated genes.
            Float minFitnessScore = function.calculate(minHilltop);
            // The neighbour list will contain all the neighbour genes for the random generated ones.
            List<Candidate> neighbours = MinHilltopUtils.generateMinHilltopNeighbours(minHilltop);

            // While iterating through neighbours the fitness score is calculated for each candidate.
            for (Candidate possibleCandidate : neighbours) {
                try {
                    fitnessScore = function.calculate(possibleCandidate);
                    // If the fitness score obtained is greater than the minFitnessScore then the minFitnessScore
                    // is updated and the Candidate is added in the MaxHilltop list of the best candidates.
                    if (fitnessScore < minFitnessScore) {
                        minFitnessScore = fitnessScore;
                        minHilltop = possibleCandidate;
                    }
                } catch (IllegalArgumentException iae) {
                    // System.out.println("The argument is out of bounds.");
                }

            }
            minHilltops.add(minHilltop);
        }
        // Finds the best candidate in the hilltop list of candidates.
        Candidate bestHilltops = null;
        Float bestHilltopsScore = Float.MIN_VALUE;
        for (Candidate hilltops : minHilltops) {
            if (function.calculate(hilltops) < bestHilltopsScore) {
                bestHilltopsScore = function.calculate(hilltops);
                bestHilltops = hilltops;
            }
        }
        System.out.println(bestHilltopsScore);
        return bestHilltops;

    }


}
