package org.elena.homework1.hillclimbing;

import sdsu.util.ConvertableBitSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Elena Hardon
 * @date 10/19/16.
 * <p>
 * Contains utility methods for Hill Climbing.
 */
public class MinHilltopUtils {

    // All genes are reprezented on 64 bits.
    public static final int GENE_SIZE = 64;

    /**
     * This method generates 64 neighbours for each gene in candidate by converting each gene into a Bitset.
     * The neighbours of a gene are 64 bitsets that have the hamming distance 1 between them and the gene.
     *
     * @param candidate Reprezents the random generated genes.
     * @return Returns a list of (64 * candidate.size) candidates.
     */
    public static List<Candidate> generateMinHilltopNeighbours(final Candidate candidate) {
        List<Candidate> candidates = new ArrayList<Candidate>();
        for (int geneIndex = 0; geneIndex < candidate.getGenes().size(); geneIndex++) {
            for (int bitIndex = 0; bitIndex < GENE_SIZE; bitIndex++) {
                // Creating the neighbour as a clone from the current candidate.
                Candidate neighbour = new Candidate();
                List<Float> neighbourGenes = new ArrayList<Float>(candidate.getGenes());
                neighbour.setGenes(neighbourGenes);

                // We must modify gene (i = geneIndex) by flipping bit (j = bitIndex)
                Float geneToBeModified = neighbour.getGenes().get(geneIndex);

                // Converting the float gene into bitset.
                ConvertableBitSet bitSetGene = ConvertableBitSet.fromFloat(geneToBeModified);

                // Flips the bit at the position bitIndex in (!bit)
                bitSetGene.flip(bitIndex);

                // Converting the gene back to float
                Float modifiedGene = bitSetGene.toFloat();

                // Put back the modified gene in the candidate
                neighbour.getGenes().set(geneIndex, modifiedGene);

                // Save the neighbour
                candidates.add(neighbour);
            }
        }
        return candidates;
    }

    public static Candidate mutatingGenes(Candidate candidate, int bitToFlip) {

        for (int geneIndex = 0; geneIndex < candidate.getGenes().size(); geneIndex++) {

            // We must modify gene (i = geneIndex) by flipping bit (j = bitToFlip)
            Float geneToBeModified = candidate.getGenes().get(geneIndex);

            // Converting the float gene into bitset.
            ConvertableBitSet bitSetGene = ConvertableBitSet.fromFloat(geneToBeModified);

            // Flips the bit at the position bitToFlip in (!bit)
            bitSetGene.flip(bitToFlip);

            // Converting the gene back to float
            Float modifiedGene = bitSetGene.toFloat();

            // Put back the modified gene in the candidate
            candidate.getGenes().set(geneIndex, modifiedGene);

        }
        return candidate;
    }

    /**
     * This method implements the genethic operator of crossover between two genes.
     *
     * @param gene1 The first float gene.
     * @param gene2 The second float gene.
     */
    public static Candidate crossover(Float gene1, Float gene2) {
        Candidate crossedGenes = new Candidate();
        ConvertableBitSet bitSetGene1, bitSetGene2, auxiliaryBitSetGene;
        Random random = new Random();
        Integer randomCrossoverNumber = random.nextInt(32);
        auxiliaryBitSetGene = ConvertableBitSet.fromFloat(gene1);
        bitSetGene1 = ConvertableBitSet.fromFloat(gene1);
        bitSetGene2 = ConvertableBitSet.fromFloat(gene2);

        for (int i = randomCrossoverNumber; i < 31; i++) {
            bitSetGene1.set(i, bitSetGene2.get(i));
            bitSetGene2.set(i, auxiliaryBitSetGene.get(i));
        }
        crossedGenes.add(bitSetGene1.toFloat());
        crossedGenes.add(bitSetGene2.toFloat());

        return crossedGenes;
    }
}
