package org.elena.homework1.hillclimbing;

import org.elena.homework0.hillclimbing.function.Function;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elena Hardon
 * @date 10/19/16.
 *
 * Calculates the best candidate that gives the bestScore using Steepest Ascent HillClimbing heuristic method.
 */
public class SteepestAscentHillClimbing {

    /**
     * This method starts by choosing a string at random(called max-hilltop).
     * Going from left to right, systematically flip each bit in the string, one at a time the neighbours are generated from the random gene.
     * If any of the resulting one-bit mutants give a fitness increase, then max-hilltop is set to the one-bit mutant giving the highest fitness increase.
     * If there is no fitness increase, then the max-hilltop is saved and the alg. goes to step 1. Otherwise, it goes to step 2 with the new max-hilltop.
     *
     * @param function           Gives the function type on which we calculate the Hill Climbing method.
     * @param numberOfIterations Gives the maximum number of iterations to be done.
     * @return This function returns the highest hilltop that was found.
     */
    public Candidate calculate(Function function, Integer numberOfIterations, Integer numbefOfGenes) {


        List<Candidate> minHilltops = new ArrayList<Candidate>();
        Candidate globalMinHilltop = null;
        boolean increaseFound;

        for (int i = 0; i < numberOfIterations; i++) {
            // Step 1.
            globalMinHilltop = function.generateRandomCandidate(numbefOfGenes);
            Float minHilltopFitnessScore = function.calculate(globalMinHilltop);

            // Step 2.
            // The neighbour list contains candidates with new genes by systematically flip each bit in the candidate, one at a time.

            increaseFound = true;
            while (increaseFound) {
                increaseFound = false;
                List<Candidate> neighbours = MinHilltopUtils.generateMinHilltopNeighbours(globalMinHilltop);

                for (Candidate neighbour : neighbours) {
                    try {
                        Float neighbourFitness = function.calculate(neighbour);

                        if (neighbourFitness < minHilltopFitnessScore) {
                            // Step 3
                            globalMinHilltop = neighbour;
                            minHilltopFitnessScore = neighbourFitness;
                            increaseFound = true;
                        }
                    } catch (IllegalArgumentException iae) {
                        //System.out.println("Candidate: " + neighbour + "is out of bounds so it is skipped!");
                    }
                }
            }
            minHilltops.add(globalMinHilltop);
        }

        // Finds the best candidate in the Hilltop list of candidates.
        Candidate bestHilltop = null;
        Float bestHilltopsScore = Float.MAX_VALUE;
        for (Candidate hilltop : minHilltops) {
            try {
                if (function.calculate(hilltop) < bestHilltopsScore) {
                    bestHilltopsScore = function.calculate(hilltop);
                    bestHilltop = hilltop;

                }
            } catch (IllegalArgumentException iae) {
                //System.out.println("Argument is out of bounds.");
            }

        }
        System.out.println(bestHilltopsScore);
        return bestHilltop;

    }
}