package org.elena.homework1.simulated.annealing;

import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework1.hillclimbing.Candidate;
import org.elena.homework1.hillclimbing.MinHilltopUtils;

import java.util.List;
import java.util.Random;

/**
 * @author Elena Hardon
 * @date 10/19/16.
 *
 * Calculates the best candidate that gives the bestScore using Simulated Annealing heuristic method.
 */


public class SimulatedAnnealing {

    /**
     * Calculates the best candidate that givest the best fitness score using Simulated Annealing heuristical method.
     *
     * @param function           The function that will be minimized.
     * @param numberOfGenes      The number of genes in a {@link Candidate}.
     * @param numberOfIterations The number of maximum iterations of the algorithm.
     * @param temperatureStep    The cooling step for the temperature.
     * @param temperature        The starting temperature.
     * @return
     */
    public static Candidate calculate(Function function, Integer numberOfGenes, Integer numberOfIterations, Float temperatureStep, Float temperature) {

        // Step 1, generate a random candidate (minHilltop).
        Candidate minHilltop = function.generateRandomCandidate(numberOfGenes);
        Float minFitness = function.calculate(minHilltop);
        Candidate randomNeighbour = null;
        Random rand;
        Integer randomBitPosition;
        while (temperature > 1) {

            for (int i = 0; i < numberOfIterations; i++) {
                // Step 2, random selecting a neighbour.
                List<Candidate> neighbours = MinHilltopUtils.generateMinHilltopNeighbours(minHilltop);
                Float fitness = null;
                rand = new Random();
                randomBitPosition = rand.nextInt(64);
                randomNeighbour = neighbours.get(randomBitPosition);
                try {
                    fitness = function.calculate(randomNeighbour);
                } catch (IllegalArgumentException iae) {
                    // System.out.println("Neighbour out of bounds.");
                    continue;
                }

                if (fitness < minFitness) {
                    minFitness = fitness;
                    minHilltop = randomNeighbour;
                } else {
                    // Step 3.
                    Float probability = rand.nextFloat();
                    Float acceptanceProbability = (float) Math.exp((minFitness - fitness) / temperature);
                    if (probability < acceptanceProbability) {
                        minFitness = fitness;
                        minHilltop = randomNeighbour;
                    }
                }
            }
            // Decreasing temperature.
            temperature = temperature * temperatureStep;
        }
        System.out.println(minFitness);
        return minHilltop;
    }

}
