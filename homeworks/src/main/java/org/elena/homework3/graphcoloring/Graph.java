package org.elena.homework3.graphcoloring;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Elena Hardon
 * @date 1/15/17.
 */

// from : http://www.geeksforgeeks.org/graph-coloring-set-2-greedy-algorithm/
public class Graph {
    private int vertices;   // No. of vertices
    private LinkedList<Integer> adjacencyList[]; //Adjacency List

    //Constructor
    Graph(int v) {
        vertices = v;
        adjacencyList = new LinkedList[v];
        for (int i = 0; i < v; ++i)
            adjacencyList[i] = new LinkedList();
    }

    //Function to add an edge into the graph
    void addEdge(int v, int w) {
        adjacencyList[v].add(w);
        adjacencyList[w].add(v); //Graph is undirected
    }

    // Assigns colors (starting from 0) to all vertices and
    // prints the assignment of colors
    void greedyColoring() {
        int result[] = new int[vertices];

        // Assign the first color to first vertex
        result[0] = 0;

        // Initialize remaining vertices-1 vertices as unassigned
        for (int u = 1; u < vertices; u++)
            result[u] = -1;  // no color is assigned to u

        // A temporary array to store the availableColors colors. True
        // value of availableColors[cr] would mean that the color cr is
        // assigned to one of its adjacent vertices
        boolean availableColors[] = new boolean[vertices];
        for (int color = 0; color < vertices; color++)
            availableColors[color] = false;

        // Assign colors to remaining vertices-1 vertices
        for (int u = 1; u < vertices; u++) {
            // Process all adjacent vertices and flag their colors
            // as unavailable
            Iterator<Integer> it = adjacencyList[u].iterator();
            while (it.hasNext()) {
                int i = it.next();
                if (result[i] != -1)
                    availableColors[result[i]] = true;
            }

            // Find the first availableColors color
            int cr;
            for (cr = 0; cr < vertices; cr++)
                if (availableColors[cr] == false)
                    break;

            result[u] = cr; // Assign the found color

            // Reset the values back to false for the next iteration
            it = adjacencyList[u].iterator();
            while (it.hasNext()) {
                int i = it.next();
                if (result[i] != -1)
                    availableColors[result[i]] = false;
            }
        }

        // print the result
        for (int u = 0; u < vertices; u++)
            System.out.println("Vertex " + u + " --->  Color " + result[u]);

    }
}
