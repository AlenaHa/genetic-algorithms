package org.elena.homework3.graphcoloring;

/**
 * @author Elena Hardon
 * @date 1/15/17.
 */
// Implementing from : https://orca-mwe.cf.ac.uk/11334/1/Lewis,_R_General_Purpose_Hill_Climbing.pdf
//GCol-Improvement-Procedure(π, ρ, I)
//        (1) i ← 0
//        (2) while (ρ != ∅ and i ≤ I)
//        (3) for (j ← 1 to |ρ|)
//        (4) for (g ← 1 to G(π))
//        (5) if (Group g is feasible for item ρ[j])
//        (6) Move item ρ[j] into group g in π
//        (7) if (ρ 6= ∅)
//        (8) repeat
//        (9) doneChange = false
//        (10) Alter π using N1 or N2
//        (11) if (the groups in π remain feasible)
//        (12) doneChange ← true
//        (13) else
//        (14) Reset the change made in Step (10)
//        (15) i ← i + 1
//        (16) until (i ≥ I or doneChange = true
// where:    I = iteration limit, , G(π) = number of groups that are contained in the permutation π.
// * the next node chosen to be coloured is determined dynamically by
// * choosing the uncoloured node that currently has the largest number of distinct colours assigned
// * to adjacent nodes (ties are then broken by choosing the node with the highest degree).
public class HillClimbingGraphColoring {
}
