package org.elena.homework3.travelingsalesman.genetic;

import java.util.ArrayList;

/**
 * @author Elena Hardon
 * @date 1/17/17.
 */
public class Population {

    // Keeps population of tours.
    ArrayList<Tour> tours;

    // Constructs a population.
    public Population(int populationSize, boolean initialise) {
        tours = new ArrayList<>(populationSize);

        // Initialising a population of tours if needed.
        if (initialise) {
            // Loop and create individuals
            for (int i = 0; i < populationSize(); i++) {
                Tour newTour = new Tour();
                newTour.generateIndividual();
                saveTour(i, newTour);
            }
        }
    }

    // Adding a tour.
    public void saveTour(int index, Tour tour) {
        tours.set(index, tour);
    }

    // Gets a tour from population
    public Tour getTour(int index) {
        return tours.get(index);
    }

    // Gets the best tour in the population
    public Tour getFittest() {
        Tour fittest = tours.get(0);
        // Loop through individuals to find fittest
        for (int i = 1; i < populationSize(); i++) {
            if (fittest.getFitness() <= getTour(i).getFitness()) {
                fittest = getTour(i);
            }
        }
        return fittest;
    }

    // Gets population size
    public int populationSize() {
        return tours.size();
    }
}
