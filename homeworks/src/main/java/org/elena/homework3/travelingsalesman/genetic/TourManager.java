package org.elena.homework3.travelingsalesman.genetic;

import java.util.ArrayList;

/**
 * @author Elena Hardon
 * @date 1/17/17.
 */
public class TourManager {

    /**
     * The array list contains the destination cities.
     */
    private static ArrayList<City> destinationCities = new ArrayList<>();

    /**
     * This function adds a new city into the destination cities.
     *
     * @param city Gives the coordinates of the city that is about to be added into the destination cities.
     */
    public static void addCity(City city) {
        destinationCities.add(city);
    }

    /**
     * This method returns the city from a given index in the destination Cities
     *
     * @param index The index for the destination cities array list.
     * @return The city from a given index in the destination Cities
     */
    public static City getCity(int index) {
        return destinationCities.get(index);
    }

    /**
     * @return The method returns the number of destination cities.
     */
    public static int numberOfCities() {
        return destinationCities.size();
    }
}
