package org.elena.homework3.travelingsalesman.greedy;

/**
 * @author Elena Hardon
 * @date 1/19/17.
 */
public class Matrix {
    // Number of rows.
    public int rowNumber;
    // Number of columns.
    public int columnNumber;
    private final int[][] matrix;

    // Create matrix.
    public Matrix(int rows, int columns) {
        this.rowNumber = rows;
        this.columnNumber = columns;
        matrix = new int[rowNumber][columnNumber];
    }

    // Copy constructor.
    private Matrix(Matrix newMatrix) {
        matrix = newMatrix.matrix;
        columnNumber = newMatrix.columnNumber;
        rowNumber = newMatrix.rowNumber;
    }

    private void setSize(int size) {
        this.columnNumber = size;
        this.rowNumber = size;
    }

    // The method returns the cost from line row, index column.
    public int getCost(int row, int column) {
        return this.matrix[row][column];
    }

    // This method sets the cost for an edge.
    public void setCost(int row, int column, int cost) {
        matrix[row][column] = cost;
    }

}
