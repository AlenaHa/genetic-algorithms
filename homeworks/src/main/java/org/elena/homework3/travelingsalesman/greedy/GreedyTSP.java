package org.elena.homework3.travelingsalesman.greedy;

import java.util.ArrayList;

/**
 * @author Elena Hardon
 * @date 1/19/17.
 */
public class GreedyTSP {
    public static final Integer MAXNUM = 30;
    public static final Integer MINIMUM = 2000;


    /**
     * This method sets costs for some edges in our graph.
     *
     * @param costMatrix An empty matrix.
     * @return The matrix filled with the costs.
     */
    public Matrix setCosts(Matrix costMatrix) {

        // The costs from a vertex to itself will be 0, meanwhile the cost for the other edges will be a higher number.
        for (int i = 0; i < costMatrix.columnNumber; i++) {
            for (int j = 0; j < costMatrix.rowNumber; j++) {
                if (i == j) {
                    costMatrix.setCost(i, j, 0);
                } else costMatrix.setCost(i, j, 1000);
            }
        }
        costMatrix.setCost(1, 2, 2);
        costMatrix.setCost(1, 3, 3);
        costMatrix.setCost(1, 4, 5);
        costMatrix.setCost(2, 1, 2);
        costMatrix.setCost(2, 3, 4);
        costMatrix.setCost(2, 4, 2);
        costMatrix.setCost(3, 1, 3);
        costMatrix.setCost(3, 2, 4);
        costMatrix.setCost(3, 4, 6);
        costMatrix.setCost(4, 1, 5);
        costMatrix.setCost(4, 2, 2);
        costMatrix.setCost(4, 3, 6);

        return costMatrix;
    }

    /**
     * This method chooses which vertex will be visited next.
     *
     * @param lastVisitedVertex The last visited vertex.
     * @param matrix            The cost matrix.
     * @param visited           The array of visited vertexes.
     * @return The best vertex to be visited and the cost to it.
     */
    public BestChoice chooseMinimumCost(int lastVisitedVertex, Matrix matrix, ArrayList<Integer> visited) {
        BestChoice bestChoice = new BestChoice();
        bestChoice.minimumCost = MINIMUM;
        bestChoice.bestVertex = -1;

        for (int j = 0; j < matrix.columnNumber; j++) {
            if (visited.get(j) == 0) {
                if (matrix.getCost(lastVisitedVertex, j) < bestChoice.minimumCost) {
                    bestChoice.minimumCost = matrix.getCost(lastVisitedVertex, j);
                    bestChoice.bestVertex = j;
                }
            }
        }
        return bestChoice;
    }


    public ArrayList<Integer> greedyCalculateTSP(Matrix matrix) {
        ArrayList<Integer> bestPath = new ArrayList<>();

        for (int j = 0; j < matrix.columnNumber; j++) {
            bestPath.add(j, 0);
        }
        int pathIndex, cost;
        int minCost = 5000;

        for (int i = 1; i < matrix.rowNumber; i++) {
            ArrayList<Integer> visited = new ArrayList<>(matrix.columnNumber);
            ArrayList<Integer> path = new ArrayList<>(matrix.columnNumber);

            // Initially no point is visited.
            for (int v = 0; v < matrix.columnNumber; v++) {
                visited.add(v, 0);
            }
            for (int j = 0; j < matrix.columnNumber; j++) {
                visited.set(j, 0);
                path.add(j, 0);
            }

            // The path will start from vertex i.
            path.set(0, i);
            // Set the vertex as visited.
            visited.set(i, 1);
            // The path has one element.
            pathIndex = 1;
            cost = 0;

            // Searching for the best vertex to be visited.
            for (int j = 1; j < matrix.rowNumber; j++) {
                BestChoice bestChoice = chooseMinimumCost(path.get(pathIndex - 1), matrix, visited);
                if (visited.get(bestChoice.bestVertex) != 1) {
                    path.set(pathIndex, bestChoice.bestVertex);
                    visited.set(bestChoice.bestVertex, 1);
                    pathIndex++;
                    cost = cost + bestChoice.minimumCost;
                } else {
                    break;
                }
            }
            // Completing the full path by adding the cost from the last vertex to the first.
            if (matrix.getCost(path.get(matrix.rowNumber - 1), i) != 0) {
                cost = cost + matrix.getCost(path.get(matrix.rowNumber - 1), i);
                path.add(pathIndex - 1, path.get(0));
            }
            System.out.println("The shortest path's cost is : " + cost);

            // If the new path has a smaller cost, the bestPath is replaced with the new one.
            if (cost < minCost) {
                for (int k = 0; k < matrix.columnNumber; k++) {
                    bestPath.set(k, path.get(k));
                    minCost = cost;
                }
            }
            System.out.println(minCost);
            for (int k = 0; k < bestPath.size(); k++) {
                System.out.println(bestPath.get(k));
            }
        }
        return bestPath;
    }

}

