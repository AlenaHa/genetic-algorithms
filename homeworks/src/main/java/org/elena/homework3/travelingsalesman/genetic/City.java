package org.elena.homework3.travelingsalesman.genetic;

/**
 * @author Elena Hardon
 * @date 1/17/17.
 */
public class City {
    // The coordinates for a city.
    int x;
    int y;

    // Constructing a random generated city
    public City() {
        x = (int) (Math.random() * 1000);
        y = (int) (Math.random() * 1000);
    }

    // Constructing a new city at chosen x, y coordinates.
    public City(int xVal, int yVal) {
        x = xVal;
        y = yVal;
    }

    // Gets city's x coordinate.
    public int getX() {
        return x;
    }

    // Gets city's y coordinate.
    public int getY() {
        return y;
    }

    /**
     * The method calculates the distance of a given city.
     *
     * @param city City's coordinates.
     * @return The method returns the distance of a given city.
     */
    public double distanceTo(City city) {
        int xDistance = Math.abs(getX() - city.getX());
        int yDistance = Math.abs(getY() - city.getY());
        double distance = Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));

        return distance;
    }

    @Override
    public String toString() {
        return getX() + ", " + getY();
    }
}
