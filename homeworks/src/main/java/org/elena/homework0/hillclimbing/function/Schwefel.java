package org.elena.homework0.hillclimbing.function;

import org.elena.homework1.hillclimbing.Candidate;

import java.util.Random;

/**
 * @author Elena Hardon
 * @date 10/23/16.
 *
 * Implements the Schwefel function.
 */
public class Schwefel implements Function {
    public static final Float UPPER_BOUND = 500.0f;
    public static final Float LOWER_BOUND = -500.0f;

    /**
     * Schwefel' s function calculates : f7(x)=sum(-x(i)·sin(sqrt(abs(x(i))))), i=1:n; -500<=x(i)<=500.
     */
    public Float calculate(Candidate candidate) {

        Float sum = 0.0f;
        for (Float argument : candidate.getGenes()) {
            if (argument > UPPER_BOUND || argument < LOWER_BOUND) {
                throw new IllegalArgumentException("The argument " + argument + " is out of bounds.");

            }
            sum = sum + (-argument) * (float) Math.sin(Math.sqrt(Math.abs(argument)));
        }
        return -sum + 450*candidate.getGenes().size();
    }

    public Candidate generateRandomCandidate(Integer numberOfGenes) {
        Candidate newRandomCandidate = new Candidate();
        Random rand = new Random();

        Float randomFloat;
        for (int i = 0; i < numberOfGenes; i++) {
            randomFloat = rand.nextFloat() * (UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND;
            newRandomCandidate.getGenes().add(randomFloat);
        }

        return newRandomCandidate;
    }
}
