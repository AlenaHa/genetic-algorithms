package org.elena.homework0.hillclimbing.function;

import org.elena.homework1.hillclimbing.Candidate;

import java.util.Random;

/**
 * @author Elena Hardon
 * @date 10/23/16.
 *
 * Implements the Six Hump Camel Back function.
 */
public class SixHumpCamelBack implements Function {
    public static final Float LOWER_BOUND_X1 = -2.0f;
    public static final Float UPPER_BOUND_X1 = 2.0f;
    public static final Float LOWER_BOUND_X2 = -3.0f;
    public static final Float UPPER_BOUND_X2 = 3.0f;

    /**
     * SixHumpCamelBack function calculates : f(x1,x2)=(4-2.1·x1^2+x1^4/3)·x1^2+x1·x2+(-4+4·x2^2)·x2^2
     * -3<=x1<=3, -2<=x2<=2.
     */
    public Float calculate(Candidate candidate) {

        if (candidate.getGenes().size() != 2) {
            throw new IllegalArgumentException("Invalid number of arguments.");
        }

        Float x1 = candidate.getGenes().get(0);
        Float x2 = candidate.getGenes().get(1);

        if (x1 > UPPER_BOUND_X1 || x1 < LOWER_BOUND_X1) {
            throw new IllegalArgumentException("The argument " + x1 + " is out of bound.");
        }

        if (x2 > UPPER_BOUND_X2 || x2 < LOWER_BOUND_X2) {
            throw new IllegalArgumentException("The argument " + x2 + " is out of bound.");
        }

        return -(float) ((4 - 2.1 * Math.pow(x1, 2) + Math.pow(x1, 4 / 3)) * Math.pow(x1, 2) + x1 * x2 + (-4 + 4 * Math.pow(x2, 2)) * Math.pow(x2, 2)) + 10 * candidate.getGenes().size();
    }

    public Candidate generateRandomCandidate(Integer numberOfGenes) {
        Candidate newRandomCandidate = new Candidate();
        Random rand1 = new Random();
        Float randomFloat1 = rand1.nextFloat() * (UPPER_BOUND_X1 - LOWER_BOUND_X1) + LOWER_BOUND_X1;
        Random rand2 = new Random();
        Float randomFloat2 = rand2.nextFloat() * (UPPER_BOUND_X2 - LOWER_BOUND_X2) + LOWER_BOUND_X2;

        newRandomCandidate.getGenes().add(randomFloat1);
        newRandomCandidate.getGenes().add(randomFloat2);

        return newRandomCandidate;
    }

}
