package org.elena.homework0.hillclimbing.function;

import org.elena.homework1.hillclimbing.Candidate;

import java.util.Random;

/**
 * @author Elena Hardon
 * @date 10/23/16.
 *
 * Implements the DeJong function.
 */
public class DeJong implements Function {
    public static final Float UPPER_BOUND = 5.12f;
    public static final Float LOWER_BOUND = -5.12f;

    //f1(x)=sum(x(i)^2), i=1:n, -5.12<=x(i)<=5.12.
    // f1(x)=sum(x(i)^2), i=1:n, -5.12<=x(i)<=5.12.

    public Float calculate(Candidate candidate) {
        Float sum = 0.0f;
        for (Float argument : candidate.getGenes()) {
            if (argument > UPPER_BOUND || argument < LOWER_BOUND) {
                throw new IllegalArgumentException("The argument " + argument + "is out of bound.");
            }
            sum = (float) (sum + Math.pow(argument, 2));
        }
        return 1/sum;
    }

    public Candidate generateRandomCandidate(Integer numberOfGenes) {

        Candidate newRandomCandidate = new Candidate();
        Random rand = new Random();

        Float randomFloat;
        for (int i = 0; i < numberOfGenes; i++) {
            randomFloat = rand.nextFloat() * (UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND;
            newRandomCandidate.getGenes().add(randomFloat);
        }

        return newRandomCandidate;
    }


}
