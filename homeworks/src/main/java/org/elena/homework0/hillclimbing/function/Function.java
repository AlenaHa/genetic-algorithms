package org.elena.homework0.hillclimbing.function;

import org.elena.homework1.hillclimbing.Candidate;

/**
 * @author Elena Hardon
 * @date 10/23/16.
 *
 * Defines a function's structure.
 */
public interface Function {
    // The EPSILON constant gives precision when evaluating the best candidate for each function.
    public static final Float EPSILON = 0.001f;


    /**
     * This method calculates with the candidate given as an parameter the specific function for each class.
     *
     * @param candidate This represents the candidate which will be evaluated.
     * @return The calculated fitness score.
     */
    public Float calculate(Candidate candidate);

    /**
     * This method generates a possible best candidate for functions.
     * @param numberOfGenes This represents the number of genes a candidate has.
     * @return The candidate.
     */

    public Candidate generateRandomCandidate(Integer numberOfGenes);

}
