package org.elena.homework0.hillclimbing.function;

import org.elena.homework1.hillclimbing.Candidate;

import java.util.Random;

/**
 * @author Elena Hardon
 * @date 10/23/16.
 *
 * Implementation of the Ackley function.
 */
public class Ackley implements Function {
    public static final Float LOWER_BOUND = -32.768f;
    public static final Float UPPER_BOUND = 32.768f;
    public static final Float a = 20.0f;
    public static final Float b = 0.2f;
    public static final Float c = (float) Math.PI;

    /**
     * Ackley's function calculates : f10(x)=-a·exp(-b·sqrt(1/n·sum(x(i)^2)))-exp(1/n·sum(cos(c·x(i))))+a+exp(1)
     * a=20; b=0.2; c=2·pi; i=1:n; -32.768<=x(i)<=32.768.
     */
    public Float calculate(Candidate candidate) {
        Float sum1 = 0.0f;
        Float sum2 = 0.0f;

        for (Float gene : candidate.getGenes()) {
            if (gene > UPPER_BOUND || gene < LOWER_BOUND) {
                throw new IllegalArgumentException("The gene " + gene + "is out of bound.");
            }
            sum1 = (float) Math.pow(gene, 2);
            sum2 = (float) Math.cos(c * gene);
        }

        return 1/(float) (-a * Math.exp(-b * Math.sqrt(sum1 / candidate.getGenes().size())) - Math.exp(sum2 / candidate.getGenes().size()) + a + Math.E);
    }

    public Candidate generateRandomCandidate(Integer numberOfGenes) {
        Candidate newRandomCandidate = new Candidate();
        Random rand = new Random();

        Float randomFloat;
        for (int i = 0; i < numberOfGenes; i++) {
            randomFloat = rand.nextFloat() * (UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND;
            newRandomCandidate.getGenes().add(randomFloat);
        }

        return newRandomCandidate;
    }
}
