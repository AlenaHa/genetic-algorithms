package org.elena.homework2;

import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework1.hillclimbing.Candidate;
import org.elena.homework1.hillclimbing.MinHilltopUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * @author Elena Hardon
 * @date 11/24/16.
 */
public class GeneticAlgorithm {

    /**
     * Generates random between (0,1]
     *
     * @return random number between [0,1]
     */
    public static Float generateRandom() {
        Random r = new Random();
        Float randomNumber;

        randomNumber = r.nextFloat() + 0.1f;
        if (randomNumber > 1f) return 1f;
        else return randomNumber;
    }


    /**
     * Using a genetic algorithm, this method calculates the best fitness score using Wheel of fortune selection and mutation as operator .
     *
     * @param function           This gives the type of calculation.
     * @param numberOfGenes      This is the number of genes to be generated for the function.
     * @param numberOfIterations This is the ending condition for the algorithm.
     * @param populationSize     The number of candidates to be generated.
     * @return
     */


    public Float calculate(Function function, Integer numberOfGenes, Integer numberOfIterations, Integer populationSize) {

        List<Candidate> population = new ArrayList<Candidate>();
        List<Candidate> survivors = new ArrayList<Candidate>();
        List<Candidate> finalPopulation = new ArrayList<Candidate>();

        List<Float> fitnessVector = new ArrayList<Float>();
        List<Float> individualSelection = new ArrayList<Float>();
        List<Float> cumulateSelection = new ArrayList<Float>();
        List<Float> crossoverGenes = new ArrayList<Float>();

        Candidate nonCrossoverCandidate = new Candidate();

        Random random = new Random();
        Float totalFitness = 0f;
        Float globalBestFitness = Float.MAX_VALUE;
        Float iterationBestFitness;
        Float CROSSOVER_RANDOM = 0.5f;

        // Random generating the first population.
        for (int k = 0; k < populationSize; k++) {
            Candidate candidate = function.generateRandomCandidate(numberOfGenes);
            population.add(candidate);
        }


        for (int l = 0; l < numberOfIterations; l++) {
            //System.out.println("Generation " + l + ":");
            //System.out.println("\tPopulation size: " + population.size());

            // This variable keeps the best fitness for the actual iteration.
            iterationBestFitness = Float.MAX_VALUE;

            // The fitness array is completed with the fitness score of each candidate from the population.
            // If the function can't calculate the fitness for a candidate because it's out of bounds, the candidate will be removed from the list.
            for (int j = 0; j < population.size(); j++) {
                try {
                    if (fitnessVector.size() >= j) {
                        fitnessVector.add(j, function.calculate(population.get(j)));
                        totalFitness = totalFitness + fitnessVector.get(j);
                    }
                } catch (IllegalArgumentException iae) {
                    population.remove(j);
                    j--;
                    // System.out.println("The argument is out of bounds.");
                }

            }


            // Applying the Wheel of fortune selection.

            for (int j = 0; j < population.size(); j++) {
                individualSelection.add(j, fitnessVector.get(j) / totalFitness);
            }

            cumulateSelection.add(0, 0.0f);
            for (int j = 0; j < population.size() - 1; j++) {
                cumulateSelection.add(j + 1, cumulateSelection.get(j) + individualSelection.get(j));
            }
            Float randomNumber;
            for (int j = 0; j < population.size(); j++) {
                randomNumber = generateRandom();
                for (int k = 0; k < population.size() - 1; k++) {
                    if (cumulateSelection.get(k) < randomNumber && cumulateSelection.get(k + 1) >= randomNumber) {
                        survivors.add(population.get(j));
                    }
                }

            }


            // Applying the mutation operator over the genes selected
            for (Candidate survivor : survivors) {
                // Random selecting a bit to flip in the gene to create a mutation.
                Integer bitToFlip = random.nextInt(64);
                MinHilltopUtils.mutatingGenes(survivor, bitToFlip);
            }


            // Crossover operation over genes.
            for (int k = 0; k < survivors.size(); k++) {
                Candidate newCandidate = new Candidate();
                // Added all selected genes in an array for crossover, and put the rest of them back in a candidate.
                for (Float gene : survivors.get(k).getGenes()) {
                    randomNumber = generateRandom();
                    if (randomNumber < CROSSOVER_RANDOM) {
                        crossoverGenes.add(gene);
                    } else {
                        // The candidate (which will be refilled until the numberOfGenes is reached back with the modified genes) is added in a candidate list.
                        newCandidate.add(gene);
                    }
                }
                finalPopulation.add(newCandidate);
                //nonCrossoverCandidate.clearAll();

            }

            Float remainingGene = null;
            if (crossoverGenes.size() % 2 == 1) {
                remainingGene = crossoverGenes.get(crossoverGenes.size() - 1);
                crossoverGenes.remove(crossoverGenes.size() - 1);
            }
            for (int i = 0; i < crossoverGenes.size() - 2; i += 2) {
                Candidate crossedGenes;
                // Applying crossover operator over two consecutive genes. The result is stored in a Candidate array.
                crossedGenes = MinHilltopUtils.crossover(crossoverGenes.get(i), crossoverGenes.get(i + 1));

                // Updating the list of genes.
                crossoverGenes.set(i, crossedGenes.getGenes().get(0));
                crossoverGenes.set(i + 1, crossedGenes.getGenes().get(1));
            }


            if (remainingGene != null) {
                crossoverGenes.add(remainingGene);
            }

            int genesToBeAdded;
            survivors.clear();


            while (crossoverGenes.size() > 0 ) {
                // The candidates from the final population will be completed with the remaining genes that were not crossed.
                for (Candidate candidate : finalPopulation ) {
                    genesToBeAdded = 0;
                    if (candidate.getGenes().size() < numberOfGenes) {
                        genesToBeAdded = numberOfGenes - candidate.getGenes().size();
                    }
                    for (int j = 0; j < genesToBeAdded; j++) {
                        candidate.add(crossoverGenes.get(j));
                    }
                    for (int m = 0; m < genesToBeAdded && crossoverGenes.size() >= genesToBeAdded; m++) {
                        crossoverGenes.remove(m);
                    }

                    if( candidate == finalPopulation.get(finalPopulation.size() - 1)
                            && finalPopulation.get(finalPopulation.size()-1).getGenes().size() == numberOfGenes ) {
                        crossoverGenes.clear();
                    }
                }

            }



            // The current population is evaluated based on the best fitness score of the current iteration.
            // Again, if there's an out of bounds result when calculating the fitness for each candidate, it will be removed from the population.
            for (Iterator<Candidate> iterator = finalPopulation.iterator(); iterator.hasNext(); ) {
                try {
                    Candidate candidate = iterator.next();
                    Float tempFitness = function.calculate(candidate);
                    if (tempFitness < iterationBestFitness) {
                        iterationBestFitness = tempFitness;
                    }
                } catch (IllegalArgumentException iae) {
                    iterator.remove();
                }
            }

            // Also, if the best fitness score of the current iteration is better than the global one, then the global fitness score is updated.
            if (globalBestFitness > iterationBestFitness) {
                globalBestFitness = iterationBestFitness;
            }

            survivors.addAll(finalPopulation);
            finalPopulation.clear();
            crossoverGenes.clear();

            // All the arrays and the total fitness are being prepared for the new generation's selection.
            totalFitness = 0f;
            population.clear();
            fitnessVector.clear();
            cumulateSelection.clear();
            individualSelection.clear();
            population.addAll(survivors);

            // The new generation that will be improved is made from the survivors of the last one, and it  is completed until the population size with random generated candidates.
            // New random generated candidates are important because they help the algorithm find better results.

            survivors.clear();

            System.out.println(iterationBestFitness);
             //System.out.println("\tIteration best candidate's fitness:" + iterationBestFitness);
             //System.out.println("\tGlobal best candidate's fitness:" + globalBestFitness);
             //System.out.print("\n");
        }


        return globalBestFitness;
    }
}