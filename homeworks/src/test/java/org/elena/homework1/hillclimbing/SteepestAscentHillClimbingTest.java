package org.elena.homework1.hillclimbing;

import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework0.hillclimbing.function.SixHumpCamelBack;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/21/16.
 *
 * Test the calculate method from {@link SteepestAscentHillClimbing} class.
 */
public class SteepestAscentHillClimbingTest {

    @Test
    public void steepestAscentHillClimbingTest() {
        Function six = new SixHumpCamelBack();
        SteepestAscentHillClimbing steepestAscentHillClimbing = new SteepestAscentHillClimbing();

        for (int i = 0; i < 30; i++) {
            Candidate bestCandidate = steepestAscentHillClimbing.calculate(six, 100, 10);

        }

    }

}
