package org.elena.homework1.hillclimbing;

import org.elena.homework0.hillclimbing.function.Ackley;
import org.elena.homework0.hillclimbing.function.Function;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/19/16.
 *
 * Test the calculate method from {@link NextAscentHillClimbing} class.
 */
public class NextAscentHillClimbingTest {
    @Test
    public void nextAscentHillClimbingTest() throws Exception {
        NextAscentHillClimbing hillClimbing = new NextAscentHillClimbing();
        Function ack = new Ackley();
        for (int i = 0; i < 30; i++) {
            Candidate bestCandidate = hillClimbing.calculate(ack, 100, 15);

        }

    }
}
