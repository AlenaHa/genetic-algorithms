package org.elena.homework1.simulated.annealing;

import org.elena.homework0.hillclimbing.function.Ackley;
import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework1.hillclimbing.Candidate;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/23/16.
 *
 * Test the calculate method from {@link SimulatedAnnealing} class.
 */
public class SimulatedAnnealingTest {
    @Test
    public void calculateTest() {
        Function ack = new Ackley();
        for (int i = 0; i < 30; i++) {
            Candidate candidate = SimulatedAnnealing.calculate(ack, 5, 100, 0.9f, 100f);

        }

    }
}
