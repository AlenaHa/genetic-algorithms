package org.elena.homework3;

import org.elena.homework3.travelingsalesman.greedy.GreedyTSP;
import org.elena.homework3.travelingsalesman.greedy.Matrix;
import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Elena Hardon
 * @date 1/19/17.
 */
public class GreedyTspTest {
    @Test
    public void calculateGreedyTspTest() {
        GreedyTSP newGreedyAlg = new GreedyTSP();
        int bestCost = 1000;
        Matrix costMatrix = new Matrix(5, 5);
        newGreedyAlg.setCosts(costMatrix);
        ArrayList<Integer> finalPath = newGreedyAlg.greedyCalculateTSP(costMatrix);

    }
}
