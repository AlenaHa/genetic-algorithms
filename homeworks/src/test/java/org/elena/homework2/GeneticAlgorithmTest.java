package org.elena.homework2;

import org.elena.homework0.hillclimbing.function.*;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 11/25/16.
 */
public class GeneticAlgorithmTest {
    @Test
    public void calculateTest() {
        Function schweffel = new SixHumpCamelBack();
        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();
        geneticAlgorithm.calculate(schweffel, 2, 100, 5);
    }

    @Test
    public void generateRandom() {
        for (int i = 0; i < 100; i++) {
            System.out.println(GeneticAlgorithm.generateRandom());
        }
    }
}
