package org.elena.homework0.hillclimbing;

import org.elena.homework0.hillclimbing.function.DeJong;
import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework1.hillclimbing.Candidate;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/5/16.
 *
 * Tests the DeJong function.
 */
public class DeJongTest {
    @Test
    public void calculateTest() {
        // f(x) = 0;
        Function deJong = new DeJong();
        Candidate arguments = new Candidate();
        arguments.add(0.0f);

        Float expectedResult = 0.0f;
        Float actualResult = deJong.calculate(arguments);

        Assert.assertEquals(expectedResult, actualResult, Function.EPSILON);

    }


    @Test(expected = IllegalArgumentException.class)
    public void calculateTestWithWrongArg() {
        // f(x) = 0;
        Function deJong = new DeJong();
        Candidate arguments = new Candidate();
        arguments.add(23.457f);

        Float expectedResult = 0.0f;
        Float actualResult = deJong.calculate(arguments);


    }
}
