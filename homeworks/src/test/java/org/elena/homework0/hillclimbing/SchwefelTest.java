package org.elena.homework0.hillclimbing;

import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework0.hillclimbing.function.Schwefel;
import org.elena.homework1.hillclimbing.Candidate;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/5/16.
 *
 * Tests the Schwefel function.
 */
public class SchwefelTest {

    @Test
    public void calculateTest() {
        //  f(x)=-n·418.9829; x(i)=420.9687, i=1:n.
        Function schwefel = new Schwefel();
        Candidate arguments = new Candidate();
        arguments.add(420.9687f);

        Float expectedResult = -arguments.getGenes().size() * 418.9829f;
        Float actualResult = schwefel.calculate(arguments);

        Assert.assertEquals(expectedResult, actualResult, Function.EPSILON);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateTestWithWrongArgumet() {
        //  f(x)=-n·418.9829; x(i)=420.9687, i=1:n.
        Function schwefel = new Schwefel();
        Candidate arguments = new Candidate();
        arguments.add(-506.000f);

        Float expectedResult = (float) (-arguments.getGenes().size() * 418.9829);
        Float actualResult = schwefel.calculate(arguments);

    }
}
