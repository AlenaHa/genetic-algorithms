package org.elena.homework0.hillclimbing;

import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework0.hillclimbing.function.SixHumpCamelBack;
import org.elena.homework1.hillclimbing.Candidate;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/5/16.
 *
 * Tests the SixHumpCamelBack function.
 */
public class SixHumpCamelBackTest {
    @Test
    public void calculateTest() {
        // f(x1,x2)=-1.0316; (x1,x2)=(-0.0898,0.7126), (0.0898,-0.7126).

        Function sixHumpCamelBack = new SixHumpCamelBack();
        Candidate candidate = new Candidate();
        candidate.add(-0.0898f);
        candidate.add(0.7126f);

        Float expectedResult = -1.0316f;
        Float actualResult = sixHumpCamelBack.calculate(candidate);

        Assert.assertEquals(expectedResult, actualResult, Function.EPSILON);

    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateTestWithWrongArgs() {
        //  f(x1,x2)=-1.0316; (x1,x2)=(-0.0898,0.7126), (0.0898,-0.7126).

        Function sixHumpCamelBack = new SixHumpCamelBack();
        Candidate candidate = new Candidate();
        candidate.add(-5.0f);
        candidate.add(4.0f);

        Float expectedResult = -1.0316f;
        Float actualResult = sixHumpCamelBack.calculate(candidate);

    }
}
