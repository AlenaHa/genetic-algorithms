package org.elena.homework0.hillclimbing;

import org.elena.homework0.hillclimbing.function.Ackley;
import org.elena.homework0.hillclimbing.function.Function;
import org.elena.homework1.hillclimbing.Candidate;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Elena Hardon
 * @date 10/5/16.
 *
 * Tests the Ackley function.
 */
public class AckleyTest {
    @Test
    public void calculateTest() {
        // f(0) = 0
        Function ackley = new Ackley();
        Candidate arguments = new Candidate();
        arguments.add(0.0f);

        Float expectedResult = 0.0f;
        Float actualResult = ackley.calculate(arguments);

        Assert.assertEquals(expectedResult, actualResult, Function.EPSILON);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateTestWithWrongArguments() {
        // -32.768<=x(i)<=32.768
        // f(0) = 0
        Function ackley = new Ackley();
        Candidate arguments = new Candidate();
        arguments.add(33.000f);

        Float expectedResult = 0.0f;
        Float actualResult = ackley.calculate(arguments);

    }


}
